﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Blackjack
{
    /// <summary>
    /// Interaction logic for DealerView.xaml
    /// </summary>
    [Serializable]
    public partial class DealerView : UserControl
    {
        GameModel gm;
        MainWindow mw;

        public DealerView(GameModel gm, MainWindow mw)
        {
            this.gm = gm; //the GameModel is passed in so we can check on status and update stuff in this view
            this.mw = mw; //pulling in the MainWindow might not be necessary.
            InitializeComponent();
            updateCards();
        }

        public void updateCards()
        {
            cardStack.Children.Clear(); //we clear any card images that might have been showing
            int i = 0; //we keep track of which index we're at, since the dealer's first card is hidden
            foreach (Hand hand in gm.dealer.getHands())
            {
                foreach (PlayingCard card in hand.getCards()) //for every card in the dealer's hand...
                {
                    //initialize image
                    Image cardFace = new Image();

                    //If it's the player's turn, we hide the first card by loading in a card back image
                    if ((gm.getPhase() == Phase.PlayerTurn || gm.getPhase() == Phase.Setup) && (i == 0))
                    {
                        cardFace.Source = new BitmapImage(new Uri("/img/b1fv.png", UriKind.Relative));
                    }
                    else //else we load in the card's actual image
                    {
                        cardFace.Source = card.getImage();
                    }
                    cardFace.Margin = new Thickness(2); //set a margin to give the cards space
                    cardStack.Children.Add(cardFace); //add the image to the stackPanel, set to increase horizontally so new cards show up to the right.
                    i++;
                }
            }
        }
    }
}
