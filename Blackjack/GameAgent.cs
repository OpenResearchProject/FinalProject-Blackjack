﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    [Serializable]
    public class GameAgent //This is a superclass that represents what our players and dealer can generally do
    {

        //private Hand hand; //each game agent has a hand of cards
        private List<Hand> hands; //actually, they can have multiple.

        public GameAgent()
        {
            //hand = new Hand();
            hands = new List<Hand>();
            hands.Add(new Hand());
        }

        public void drawHand(Deck deck, Hand hand) //draws a full hand (default 2 cards)
        {
            for (int i = 0; i < hand.getStartingHandSize(); i++)
            {
                hand.AddCardToHand(deck.DealCardFromDeck());
            }
        }

        public void drawCard(Deck deck, Hand hand) //draws a single card
        {
            hand.AddCardToHand(deck.DealCardFromDeck());
        }

        //public Hand getHand() //returns full Hand object of this agent, might want to obfuscate that a bit
        //{
        //    return hand;
        //}

        public List<Hand> getHands()
        {
            return hands;
        }

        public void discardHand(Deck deck, Hand hand) //discards all cards in hand
        {
            if (hand.getValue() > 0)
            {
                deck.addToDiscard(hand.discardHand());
            }
        }

        public void splitHand(Deck deck, Hand hand)
        {
            //create a new hand
            Hand newHand = new Hand();

            //give it one of the old hand's cards
            newHand.AddCardToHand(hand.getCards()[0]);
            hand.getCards().Remove(hand.getCards()[0]);

            //give the new hand a bet amount
            newHand.setBet(hand.getBet());

            //draw up for each hand
            drawCard(deck, hand);
            drawCard(deck, newHand);

            //add it to the player's Hands
            hands.Add(newHand);
        }

        public void clearHands(Deck deck)
        {
            foreach(Hand hand in hands)
            {
                discardHand(deck, hand);
            }
            hands.Clear();
            hands.Add(new Hand());
        }
    }
}
