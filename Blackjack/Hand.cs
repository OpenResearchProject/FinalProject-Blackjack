using System;
using System.Collections.Generic;

namespace Blackjack
{
    [Serializable]
    public class Hand
    {
        private int handSize;
        private List<PlayingCard> cards;
        private double bet;
        private bool hasBlackJack;
        private Results won;
        private HandStatus handStatus;

        public Hand()
        {
            //Basic initialization of default Blackjack stuff
            handSize = 2;
            cards = new List<PlayingCard>(handSize);
            bet = 0; //hand has a bet since splits have seperate bets per hand
            handStatus = HandStatus.Active;
        }

		public void AddCardToHand(PlayingCard cardDealt)
		{
            this.cards.Add(cardDealt);
		}

		public override string ToString()
		{
			string result = "";
			foreach (PlayingCard card in this.cards)
			{
				result += card.ToString() + "\n";
			}

			return result;
		}

        public int getStartingHandSize()
        {
            return handSize;
        }

        public int getHandSize()
        {
            return cards.Count;
        }

        public List<PlayingCard> discardHand()
        {
            List<PlayingCard> tempCards = new List<PlayingCard>();
            foreach (PlayingCard card in cards){
                tempCards.Add(card);
            }
            cards.Clear();
            return tempCards;
        }
        public void setBet(double playerBet)
        {
            bet = playerBet;
        }
        public double getBet()
        {
            return bet;
        }
        public void clearBet()
        {
            bet = 0;
        }

        public int getValue()
        {
            //We've got a bunch of stuff here to store important blackjack rules
            int value = 0;
            int aceValue = 0;
            bool aceToggle = false;
            bool jackToggle = false;
            hasBlackJack = false;
            foreach (PlayingCard card in cards)
            {
                switch (card.CardValue)
                {
                    case Value.Ace:
                        aceValue += 1; //aceValue is seperate since Aces can be valued in different ways in BlackJack
                        aceToggle = true;
                        break;
                    case Value.Two:
                        value += 2;
                        break;
                    case Value.Three:
                        value += 3;
                        break;
                    case Value.Four:
                        value += 4;
                        break;
                    case Value.Five:
                        value += 5;
                        break;
                    case Value.Six:
                        value += 6;
                        break;
                    case Value.Seven:
                        value += 7;
                        break;
                    case Value.Eight:
                        value += 8;
                        break;
                    case Value.Nine:
                        value += 9;
                        break;
                    case Value.Ten:
                        jackToggle = true;
                        value += 10;
                        break;
                    case Value.Jack:
                        jackToggle = true;
                        value += 10;
                        break;
                    case Value.King:
                        jackToggle = true;
                        value += 10;
                        break;
                    case Value.Queen:
                        jackToggle = true;
                        value += 10;
                        break;
                    default:
                        break;
                }
            }
            
            //Alright, here's the weird Ace handling stuff.
            if (aceToggle && (aceValue < 11) && ((value + aceValue + 10) <= 21)) //If we have an ace, the extra value hasn't been already added to the Ace, and it wouldn't bust the player, add the extra 10 points
            {
                aceValue += 10;
            }

            //check for BlackJack
            if (aceToggle && jackToggle && (cards.Count == 2)) //we have just two cards, one of which is a face card and the other is an ace
            {
                hasBlackJack = true;
                handStatus = HandStatus.Stay;
            }

            return value + aceValue;
        }

        public List<PlayingCard> getCards() // temp for testing
        {
            return cards;
        }

        public bool blackJackCheck()
        {
            int temp = getValue(); //need to make sure we run this for the check. Sloppy but effective
            return hasBlackJack;
        }

        public void setWin(Results result)
        {
            won = result;
        }
        public Results getWin()
        {
            return won;
        }
        public void setHandStatus(HandStatus hs)
        {
            handStatus = hs;
        }
        public HandStatus getHandStatus()
        {
            return handStatus;
        }
	}
}