
using System;

namespace Blackjack
{
    [Serializable]
    public enum Suit { Clubs, Diamonds, Hearts, Spades }
}