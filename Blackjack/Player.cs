﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    [Serializable]
    public class Player : GameAgent //gets basic functionality from GameAgent class, specific roles and info can be included here
    {
        double cash;
        double debt;
        double wins;
        double loses;
        double ties;
        String name;

        public Player(double winnings, double loan) : base() //base() runs the superclass constructor
        {
            this.cash = cash + winnings + loan;
            this.debt = debt + loan;
            wins = 0;
            loses = 0;
            ties = 0;
            name = "Player";
        }
        public double getCurrentCash()
        {
            return cash;
        }
        public double updateCash(double winnings)
        {
            this.cash = cash + winnings;
            return cash;
        }
        public double updateDebt(double loan)
        {
            this.debt = debt + loan;
            cash += loan;
            return debt;
        }
        public void addDebt(double loan)
        {
            debt = debt + loan;
            cash += loan;
        }
        public double getDebt()
        {
            return debt;
        }
        public void setName(String name)
        {
            this.name = name;
        }
        public String getName()
        {
            return name;
        }
        public void addWin()
        {
            wins++;
        }
        public void addLose()
        {
            loses++;
        }
        public void addTie()
        {
            ties++;
        }
        public double getWins()
        {
            return wins;
        }
        public double getLoses()
        {
            return loses;
        }
        public double getTies()
        {
            return ties;
        }
    }
}
