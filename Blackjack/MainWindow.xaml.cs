﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

namespace Blackjack
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [Serializable]
    public partial class MainWindow : Window
    {
        //initialize stuff
        GameModel gm;
        PlayerStatusView psv;
        private String username, password; // For save files

        public MainWindow()
        {
            InitializeComponent();

            this.Hide(); // Hide the MainWindow while the Login window is up
            Login login = new Login(this); //pass MainWindow reference to login to call startGame later
            login.Show(); // Actually show our Login...
        }

        public void startGame(GameModel gm, String username, String password) //called by login after user signs in
        {
            this.gm = gm; // Login sets our GameModel since it loads in from a file
            this.username = username;
            this.password = password;
            this.Show(); // Show our MainWindow again now that the Login has closed

            //gm.startRound();
            //psv = new PlayerStatusView(gm, this);
            //playerStatusGrid.Children.Add(psv); //this is user control with basic Player info (Name, cash, bet)
            updateView();
            gm.player.setName(username);
            playerNameLabel.Content = gm.player.getName();
        }

        public void updateView()
        {
            double currentBet;

            updateDealerValue();

            //Make sure dealer value is hidden
            dealerValueLabel.Visibility = Visibility.Hidden;
            dealerValueAmountLabel.Visibility = Visibility.Hidden;

            //Display cash:
            cashAmountLabel.Content = gm.player.getCurrentCash().ToString();
            currentBet = 0;
            foreach (Hand hand in gm.player.getHands())
            {
                currentBet += hand.getBet();
            }
            currentBetAmountLabel.Content = currentBet.ToString();

            //general updates - stuff we want to display whenever
            losesAmountLabel.Content = gm.player.getLoses().ToString();
            winAmountLabel.Content = gm.player.getWins().ToString();
            tiesAmountLabel.Content = gm.player.getTies().ToString();
            playerDebtAmountLabel.Content = gm.player.getDebt();

            //Make Cards visible
            makeDealerCardsVisible();

            //Check phase state and load in different UI elements
            if (gm.getPhase() == Phase.Setup)
            {
                updateVisiblePlayerCards();
                //check for blackjack and insurance stuff
                gm.performAfterSetup();
                updateView();
            }
            else if (gm.getPhase() == Phase.Results)
            {
                //Make sure dealer value is visible
                dealerValueLabel.Visibility = Visibility.Visible;
                dealerValueAmountLabel.Visibility = Visibility.Visible;

                updateVisiblePlayerCards();
                playAgainButton.IsEnabled = true;
                disableBetting();
                gm.endRound();
                updateView();
            } else if (gm.getPhase() == Phase.DealerTurn)
            {
                gm.dealerTurn();
                //We don't have to do any view stuff here, since everything happens too fast for the player to notice
                updateView();
            } else if (gm.getPhase() == Phase.Betting)
            {
                //Make sure dealer value is visible
                dealerValueLabel.Visibility = Visibility.Visible;
                dealerValueAmountLabel.Visibility = Visibility.Visible;

                enableBetting();
                playAgainButton.IsEnabled = false;
            } else if (gm.getPhase() == Phase.PlayerTurn)
            {
                updateVisiblePlayerCards();
                playAgainButton.IsEnabled = false;
                disableBetting();
            }

            //general updates - stuff we want to display whenever
            losesAmountLabel.Content = gm.player.getLoses().ToString();
            winAmountLabel.Content = gm.player.getWins().ToString();
            tiesAmountLabel.Content = gm.player.getTies().ToString();
            playerDebtAmountLabel.Content = gm.player.getDebt();

        }

        private void updateDealerValue()
        {
            dealerValueAmountLabel.Content = gm.dealer.getHands()[0].getValue().ToString();
        }

        private void makeDealerCardsVisible()
        {
            //visible cards
            dealerHand.Children.Clear();
            dealerHand.Children.Add(new DealerView(gm, this));
        }

        private void updateVisiblePlayerCards()
        {
            //player's cards visible
            //clear previous handViews (handHolder is a StackPanel that holds the player's hands)
            handHolder.Children.Clear();

            //enable Hand view
            foreach (Hand hand in gm.player.getHands())
            {
                HandView hv = new HandView(gm, this, hand);
                //Activate buttons based on Status
                if (hand.getHandStatus() != HandStatus.Active)
                {
                    hv.disableButtons();
                }
                //Add to view
                handHolder.Children.Add(hv);
            }
        }

        private void playAgainButton_Click(object sender, RoutedEventArgs e)
        {
            gm.setPhase(Phase.Betting);
            updateView();
        }

        private void wager5_Click(object sender, RoutedEventArgs e)
        {
            //currWager.Text = "5";
            gm.startRound(5);
            updateView();
        }

        private void wager10_Click(object sender, RoutedEventArgs e)
        {
            //currWager.Text = "10";
            gm.startRound(10);
            updateView();
        }

        private void wager15_Click(object sender, RoutedEventArgs e)
        {
            //currWager.Text = "15";
            gm.startRound(15);
            updateView();
        }

        private void disableBetting()
        {
            //disable buttons
            fiveDollarRadio.IsEnabled = false;
            tenDollarRadio.IsEnabled = false;
            fifteenDollarRadio.IsEnabled = false;
            twentyDollarRadio.IsEnabled = false;
            hundredDollarRadio.IsEnabled = false;
            betButton.IsEnabled = false;
        }

        private void enableBetting()
        {
            //enable buttons
            fiveDollarRadio.IsEnabled = true;
            tenDollarRadio.IsEnabled = true;
            fifteenDollarRadio.IsEnabled = true;
            twentyDollarRadio.IsEnabled = true;
            hundredDollarRadio.IsEnabled = true;
            betButton.IsEnabled = true;
        }

        private void betButton_Click(object sender, RoutedEventArgs e)
        {
            int betAmount = 0;

            if (fiveDollarRadio.IsChecked == true)
            {
                //gm.startRound(5);
                betAmount = 5;
            } else if (tenDollarRadio.IsChecked == true)
            {
                //gm.startRound(10);
                betAmount = 10;
            } else if (fifteenDollarRadio.IsChecked == true)
            {
                //gm.startRound(15);
                betAmount = 15;
            } else if (twentyDollarRadio.IsChecked == true)
            {
                //gm.startRound(20);
                betAmount = 20;
            } else if (hundredDollarRadio.IsChecked == true)
            {
                //gm.startRound(100);
                betAmount = 100;
            } else
            {
                return;
            }

            if (gm.player.getCurrentCash() < betAmount)
            {
                MessageBox.Show("You don't have enough money for that bet.  Pick a smaller bet or borrow money from the Bank.");
            } else
            {
                //gm.clearHands();
                //updateView();
                gm.startRound(betAmount);
                betButton.IsEnabled = false;
            }

            updateView();
        }

        private void loanButton_Click(object sender, RoutedEventArgs e)
        {
            gm.player.addDebt(500);
            updateView();
        }

        private void payOffButton_Click(object sender, RoutedEventArgs e)
        {
            if (gm.player.getDebt() > 0)
            {
                if (gm.player.getCurrentCash() > 500)
                {
                    gm.player.addDebt(-500);
                } else
                {
                    MessageBox.Show("You don't have the cash to make a payment.");
                }
            } else
            {
                MessageBox.Show("You've got no debt to pay! Try playing more BlackJack to fix that.");
            }

            updateView();
        }

        private void saveAndQuit_Click_1(object sender, RoutedEventArgs e)
        {
            String curFile = Directory.GetCurrentDirectory() + "\\gameData_" + username + ".save";
            File.Delete(curFile + "s");

            //Write to File:
            IFormatter formatter = new BinaryFormatter();
            Stream stream = new FileStream(curFile, FileMode.Create, FileAccess.Write, FileShare.None);
            formatter.Serialize(stream, gm);
            stream.Close();

            Cryptology.encryptFile(curFile, password);

            password = "derp";
            password = null;

            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }
        
    }
}
