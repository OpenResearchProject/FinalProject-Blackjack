﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    [Serializable]
    public class DiscardPile
    {
        List<PlayingCard> cards;

        public DiscardPile()
        {
            cards = new List<PlayingCard>();
        }

        public void addToDiscard(List<PlayingCard> cardsToDiscard)
        {
            cards.AddRange(cardsToDiscard);
        }

        public List<PlayingCard> getCards()
        {
            return cards;
        }

        public void clearDiscard()
        {
            cards.Clear();
        }
    }
}
