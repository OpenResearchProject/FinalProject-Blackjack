﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace Blackjack
{
    /// <summary>
    /// Interaction logic for InsuranceWindow.xaml
    /// </summary>
    public partial class InsuranceWindow : Window
    {
        GameModel gameModel;
        double betAmount;

        public InsuranceWindow(GameModel gm)
        {
            gameModel = gm;

            InitializeComponent();

            //Get the bet amount for Insurance
            betAmount = gameModel.player.getHands()[0].getBet() / 2;

            //Display the amount player would need to pay to buy Insurance
            insuranceBetAmountLabel.Content = betAmount.ToString();

        }

        private void buyInsuranceButton_Click(object sender, RoutedEventArgs e)
        {
            if (gameModel.player.getCurrentCash() < betAmount)
            {
                MessageBox.Show("Sorry, you don't have enough cash to buy Insurance");
            } else
            {
                gameModel.player.updateCash(-betAmount);
                if (gameModel.dealer.getHands()[0].blackJackCheck())
                {
                    gameModel.player.updateCash(2 * betAmount);
                }
            }
            this.Close();

        }

        private void noInsuranceButton_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
