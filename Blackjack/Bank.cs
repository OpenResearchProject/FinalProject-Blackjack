﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    [Serializable]
    public class Bank
    {
        private double bet;
        private double debt;
        private double winnings;

        public Bank()
        {
            bet = 0;
            debt = 0;
            winnings = 0;
        }
        public void setBet(double playerBet)
        {
            bet = playerBet;
        }
        public double getBet()
        {
            return bet;
        }
        public double updateBet(double playerBet)
        {
            this.bet = bet * 2;
            return bet;
        }

        public double blackJackBet(double playerBet)
        {
            this.bet = bet * 2.5;
            return bet;
        }

        public void clearBet()
        {
            this.bet = 0;
        }
    }
}
