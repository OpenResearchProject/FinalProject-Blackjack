﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Blackjack
{
    /// <summary>
    /// Interaction logic for HandView.xaml
    /// </summary>
    [Serializable]
    public partial class HandView : UserControl
    {
        GameModel gameModel;
        MainWindow mainWindow;
        Hand hand;

        public HandView(GameModel gm, MainWindow mw, Hand hnd)
        {
            gameModel = gm;
            mainWindow = mw;
            hand = hnd;

            InitializeComponent();
            updateCards();
        }

        public void disableButtons() //an easy way to disable all the command buttons when it's not the player's turn
        {
            hitButton.IsEnabled = false;
            stayButton.IsEnabled = false;
            splitButton.IsEnabled = false;
            doubleDownButton.IsEnabled = false;
            surrenderButton.IsEnabled = false;
        }

        private void updateCards()
        {
            foreach (PlayingCard card in hand.getCards())
            {
                Image cardFace = new Image();
                cardFace.Source = card.getImage();
                cardFace.Margin = new Thickness(2);
                cardGrid.Children.Add(cardFace);
            }
            if ((hand.getValue() == 21) && (hand.getHandSize() == 2))
            {
                handValueAmountLabel.Content = "BlackJack!";
            }
            else
            {
                handValueAmountLabel.Content = hand.getValue();
            }
        }

        private void hitButton_Click(object sender, RoutedEventArgs e)
        {
            gameModel.playerHit(hand);
            mainWindow.updateView();
        }

        private void stayButton_Click(object sender, RoutedEventArgs e)
        {
            hand.setHandStatus(HandStatus.Stay);
            //gameModel.dealerTurn();
            gameModel.checkTurn();
            mainWindow.updateView();
        }

        private void surrenderButton_Click(object sender, RoutedEventArgs e)
        {
            if (hand.getHandSize() <= 2)
            {
                gameModel.surrender(hand);
                gameModel.checkTurn();
                mainWindow.updateView();
            } else
            {
                MessageBox.Show("Can only surrender on first turn");
            }
        }

        private void doubleDownButton_Click(object sender, RoutedEventArgs e)
        {
            if (hand.getHandSize() == 2)
            {
                if (gameModel.player.getCurrentCash() >= hand.getBet())
                {
                    gameModel.doubleDown(hand);
                    mainWindow.updateView();
                } else
                {
                    MessageBox.Show("You don't have enough Cash to Double Down");
                }
            } else
            {
                MessageBox.Show("Can't Double Down if you've already taken a Hit");
            }
        }

        private void splitButton_Click(object sender, RoutedEventArgs e)
        {
            //Check conditions for split
            if ((hand.getCards().Count == 2) && (hand.getCards()[0].CardValue == hand.getCards()[1].CardValue))
            {
                //Check that player has enough money to split (requires bet equal to the current hand)
                if (gameModel.player.getCurrentCash() >= hand.getBet())
                {
                    //split the hand
                    gameModel.split(hand);
                    mainWindow.updateView();
                }
                else
                {
                    MessageBox.Show("Not Enough Money to Split");
                }
            }
            else
            {
                MessageBox.Show("Can't split cards that aren't equal Values");
            }

        }
    }
}
