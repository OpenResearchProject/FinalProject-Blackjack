﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    public partial class Login : Form
    {
        String aPassword = "";
        MainWindow mw;

        public Login(MainWindow mw)
        {
            this.mw = mw;
            InitializeComponent();
        }

        protected override void OnFormClosing(FormClosingEventArgs e)
        {
            // User hit the "X" Close button, so close whole program instead of just this window:
            base.OnFormClosing(e);

            if (System.Windows.Forms.Application.MessageLoop)
            {
                // WinForms app
                System.Windows.Forms.Application.Exit();
            }
            else
            {
                // Console app
                System.Environment.Exit(1);
            }
        }

        private void newUserButton_Click_1(object sender, EventArgs e)
        {
            if (usernameBox.Text.Length < 4)
            {
                MessageBox.Show("Username length must be at least 4 characters long!");
            }
            else
            {
                aPassword = passwordBox.Text;
                aPassword = aPassword.Replace(" ", "");
                passwordBox.Text = "derp";
                passwordBox.Text = null;

                if (aPassword.Length < 8)
                {
                    MessageBox.Show("Password must be at least 8 characters long without spaces!");
                    aPassword = "derp";
                    aPassword = null;
                }
                else
                {
                    aPassword = Cryptology.sha256sum(aPassword);

                    String curFile = Directory.GetCurrentDirectory() + "\\gameData_" + usernameBox.Text + ".save";

                    if (File.Exists(curFile + "s"))
                    {
                        MessageBox.Show("A user with that name already exists!");
                    }
                    else
                    {
                        //New Game:
                        GameModel gm = new GameModel();

                        //Write to File:
                        IFormatter formatter = new BinaryFormatter();
                        Stream stream = new FileStream(curFile, FileMode.Create, FileAccess.Write, FileShare.None);
                        formatter.Serialize(stream, gm);
                        stream.Close();

                        Cryptology.encryptFile(curFile, aPassword);

                        mw.startGame(gm, usernameBox.Text, aPassword);
                        aPassword = "derp";
                        aPassword = null;
                        this.Hide();
                    }
                }
            }
        }

        private void loginButton_Click_1(object sender, EventArgs e)
        {
            String curFile = Directory.GetCurrentDirectory() + "\\gameData_" + usernameBox.Text + ".save";

            if (File.Exists(curFile + "s"))
            {
                aPassword = passwordBox.Text;
                aPassword = aPassword.Replace(" ", "");
                aPassword = Cryptology.sha256sum(aPassword);
                passwordBox.Text = "derp";
                passwordBox.Text = null;

                try
                {
                    // Decrypt our save file:
                    // It throws an error if the password is wrong.
                    Cryptology.decryptFile(curFile, aPassword);
                }
                catch (InvalidDataException)
                {
                    MessageBox.Show("Invalid Password!\n" + passwordBox.Text);
                    File.Delete(curFile);
                    return;
                }

                //Read File:
                IFormatter formatter = new BinaryFormatter();
                Stream stream = new FileStream(curFile, FileMode.Open, FileAccess.Read, FileShare.Read);
                GameModel gm = (GameModel)formatter.Deserialize(stream);
                stream.Close();

                //Re-encrypt our save file:
                Cryptology.encryptFile(curFile, aPassword);

                //create new randomizer for cards
                gm.updateRandomizer(new Random());

                //start game up
                mw.startGame(gm, usernameBox.Text, aPassword);
                aPassword = "derp";
                aPassword = null;
                this.Hide();

            }
            else
            {
                MessageBox.Show("A user with that name couldn't be found.");
            }
        }
        
    }
}
