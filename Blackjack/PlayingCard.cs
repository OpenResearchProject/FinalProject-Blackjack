
using System;
using System.Runtime.Serialization;
using System.Windows.Media.Imaging;

namespace Blackjack
{
    [Serializable]
    public class PlayingCard
	{
        private readonly Suit suit;
        private readonly Value value;
        [NonSerialized] private BitmapImage cardFace;
        [NonSerialized] private Boolean isLoaded; // Check if cards have been loaded.

		public PlayingCard(Suit s, Value v)
		{
			this.suit = s;
			this.value = v;

            //initiating image junk
            cardFace = new BitmapImage();
            cardFace.BeginInit();

            switch (s)
            {
                case Suit.Clubs:
                    cardFace.UriSource = new Uri("/img/c" + valueCheck(v) + ".png", UriKind.Relative);
                    break;
                case Suit.Diamonds:
                    cardFace.UriSource = new Uri("/img/d" + valueCheck(v) + ".png", UriKind.Relative);
                    break;
                case Suit.Hearts:
                    cardFace.UriSource = new Uri("/img/h" + valueCheck(v) + ".png", UriKind.Relative);
                    break;
                case Suit.Spades:
                    cardFace.UriSource = new Uri("/img/s" + valueCheck(v) + ".png", UriKind.Relative);
                    break;
            }
            isLoaded = true; // Cards loaded normally, so true.
            cardFace.EndInit();
		}

        private String valueCheck(Value v)
        {
            String value = "";

            switch (v)
            {
                case Value.Ace:
                    value = "1";
                    break;
                case Value.Two:
                    value = "2";
                    break;
                case Value.Three:
                    value = "3";
                    break;
                case Value.Four:
                    value = "4";
                    break;
                case Value.Five:
                    value = "5";
                    break;
                case Value.Six:
                    value = "6";
                    break;
                case Value.Seven:
                    value = "7";
                    break;
                case Value.Eight:
                    value = "8";
                    break;
                case Value.Nine:
                    value = "9";
                    break;
                case Value.Ten:
                    value = "10";
                    break;
                case Value.Jack:
                    value = "j";
                    break;
                case Value.Queen:
                    value = "q";
                    break;
                case Value.King:
                    value = "k";
                    break;
            }

            return value;

        }

        public override string ToString()
		{
            string result = string.Format("{0} of {1}", this.value, this.suit);
			return result;
		}

        public Suit CardSuit
        {
            get
            {
                return this.suit;
            }
        }

        public Value CardValue
        {
            get
            {
                return this.value;
            }
        }

        public BitmapImage getImage()
        {
            if (!isLoaded) // If the cards haven't been loaded up. This happens when you load a save file since it is NonSerialized.
            {
                cardFace = new BitmapImage();
                cardFace.BeginInit();

                switch (suit)
                {
                    case Suit.Clubs:
                        cardFace.UriSource = new Uri("/img/c" + valueCheck(value) + ".png", UriKind.Relative);
                        break;
                    case Suit.Diamonds:
                        cardFace.UriSource = new Uri("/img/d" + valueCheck(value) + ".png", UriKind.Relative);
                        break;
                    case Suit.Hearts:
                        cardFace.UriSource = new Uri("/img/h" + valueCheck(value) + ".png", UriKind.Relative);
                        break;
                    case Suit.Spades:
                        cardFace.UriSource = new Uri("/img/s" + valueCheck(value) + ".png", UriKind.Relative);
                        break;
                }
                isLoaded = true; // Cards loaded normally, so true.
                cardFace.EndInit();

                isLoaded = true;
            }
            return cardFace;
        }
	}
}