﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Blackjack
{
    [Serializable]
    public class GameModel
    {
        private Deck deck;
        public Dealer dealer; //only public now for testing, GameModel will eventually be self-contained
        public Player player; //only public now for testing
        private Phase phase;

        public GameModel()
        {
            //initialize stuff
            dealer = new Dealer();
            player = new Player(500, 0);
            setPhase(Phase.Betting);
            deck = new Deck();
            
        }

        public void setPhase(Phase p)
        {
            //change the phase
            phase = p;
            //update listeners?

        }

        public Phase getPhase()
        {
            return phase;
        }

        public void startRound(double bet) //start a new round
        {

            //clear previous round
            clearHands();

            //set bet amount
            foreach (Hand hand in player.getHands())
            {
                hand.setBet(bet);
            }

            //Take player's money
            player.updateCash(-bet);

            //players draw up
            foreach (Hand hand in player.getHands())
            {
                player.drawHand(deck, hand);
            }
            foreach (Hand hand in dealer.getHands())
            {
                dealer.drawHand(deck, hand);
            }

            setPhase(Phase.Setup);
  
        }

        public void clearHands()
        {
            player.clearHands(deck);
            dealer.clearHands(deck);
        }

        public void performAfterSetup()
        {
            //Check for Insurance and give player Insurance Option
            if (dealer.getHands()[0].getCards()[1].CardValue == Value.Ace)
            {
                //give player option to buy insurance before any other actions
                //by opening up the Insurance window
                InsuranceWindow iw = new InsuranceWindow(this);
                iw.ShowDialog();
            }

            //Check for Blackjack
            if (dealer.getHands()[0].blackJackCheck())
            {
                //if Dealer has blackjack, we skip to results
                player.getHands()[0].setHandStatus(HandStatus.Stay);
                setPhase(Phase.Results);
                MessageBox.Show("Dealer has BlackJack.  Round is over.  Please bet again.");
            }
            else
            {
                bool blackJackEnd = true;
                foreach (Hand hand in player.getHands())
                {
                    hand.blackJackCheck();
                    if (!hand.blackJackCheck())
                    {
                        blackJackEnd = false;
                    }
                }
                if (blackJackEnd)
                {
                    setPhase(Phase.Results);
                }
                else
                {
                    //go to Player Control (done in the view)
                    setPhase(Phase.PlayerTurn);
                }
            }
        }

        public void endRound() //ends last round
        {
            getWinner();
            foreach (Hand hand in player.getHands())
            {
                //payout winnings or deduct losses
                if (hand.getWin() == Results.Win)
                {
                    player.updateCash(hand.getBet() * 2); //gives player twice their bet back, needs to be fixed to actual ratios.
                }
                else if (hand.getWin() == Results.Lose)
                {
                    //we're going to take money from the player as soon as they bet, so we don't need to take it here
                }
                else if (hand.getWin() == Results.Tie) //for a draw...
                {
                    player.updateCash(hand.getBet()); //player gets their money back.  Should probably double check rules here.
                }
                else if (hand.getWin() == Results.Surrender)
                {
                    //Surrender action handles the money here, so we don't have to do anything.
                }

                //Pays out insurance if player has it and Dealer had Blackjack
                //Might not have to do this here if we can handle it all inside the InsuranceWindow

                //reset bet info
                hand.clearBet();
            }

            //Go to Betting Control (done in the view)
            setPhase(Phase.Betting);
        }

        public void playerHit(Hand hand)
        {
            //player draws a card
            player.drawCard(deck, hand);

            //check to see if player busts
            bustCheck(hand); //marks hand as busted
            //check to see if all hands are finished (Busted or whatever)
            checkTurn();
        }

        public void dealerTurn()
        {
            //check to see if player has busted all hands.  If so, the dealer doesn't have to draw
            //first we assume player has busted all hands
            bool playerBust = true;
            //now we run through all hands to check them
            foreach(Hand hand in player.getHands())
            {
                //if a hand didn't actually bust...
                if (bustCheck(hand) == false)
                {
                    //then we know the player didn't actually bust all their hands, and the dealer will draw
                    playerBust = false;
                }
            }
            while ((dealer.getHands()[0].getValue()) < 17 && (playerBust == false))
            {
                dealer.drawCard(deck, dealer.getHands()[0]);
            }

            setPhase(Phase.Results);
        }

        public PlayingCard dealCard()
        {
            return deck.DealCardFromDeck();
        }

        public void placeInDiscard(List<PlayingCard> cards)
        {
            deck.addToDiscard(cards);
        }

        private bool bustCheck(Hand h) //checks to see if hand value is over 21
        {
            if (h.getValue() > 21)
            {
                h.setHandStatus(HandStatus.Bust);
            }
            return (h.getValue() > 21);
        }

        public void getWinner() //returns the winner
        {
            foreach (Hand hand in player.getHands())
            {
                int playerScore = hand.getValue();
                int dealerScore = dealer.getHands()[0].getValue();

                if (hand.getWin() == Results.Surrender)
                {
                    //We don't do anything, they surrendered, so we dont count it as a win, lose or tie
                }
                else if ((playerScore <= 21) && ((playerScore > dealerScore) || (dealerScore > 21)))
                {
                    hand.setWin(Results.Win);
                    player.addWin();
                }
                else if ((dealerScore <= 21) && (dealerScore != playerScore))
                {
                    hand.setWin(Results.Lose);
                    player.addLose();
                }
                else
                {
                    hand.setWin(Results.Tie);
                    player.addTie();
                }
            }
        }

        public void surrender(Hand hand)
        {
            player.updateCash(hand.getBet() * .5); //gives player half their bet back
            //reset bet info
            hand.clearBet();
            hand.setWin(Results.Surrender);
            hand.setHandStatus(HandStatus.Surrender);
            //setPhase(Phase.Results);
            checkTurn();
        }

        public void doubleDown(Hand hand)
        {
            player.updateCash(-hand.getBet());
            hand.setBet(hand.getBet() * 2);
            playerHit(hand);
            //Disable further betting
            hand.setHandStatus(HandStatus.Stay);
            checkTurn();
        }

        public void split(Hand hand)
        {
            //condition testing is done in the hand view.  We trust the player has enough money and the right cards here.
            //create a new hand
            player.splitHand(deck, hand);
            //take the money for the new bet
            player.updateCash(-hand.getBet());
            checkTurn();
        }

        internal void checkTurn()
        {
            bool playerTurn = false;
            foreach(Hand hand in player.getHands())
            {
                if (hand.getHandStatus() == HandStatus.Active)
                {
                    playerTurn = true;
                }
            }

            if (!playerTurn)
            {
                dealerTurn();
            }
        }

        public void updateRandomizer(Random rando)
        {
            deck.updateRandomizer(rando);
        }
    }
}
