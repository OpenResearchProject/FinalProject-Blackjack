﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Blackjack
{
    [Serializable]
    public enum HandStatus {Bust, Stay, Active, Surrender}
}
