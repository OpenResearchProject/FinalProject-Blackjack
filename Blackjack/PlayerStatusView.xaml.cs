﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Blackjack
{
    /// <summary>
    /// Interaction logic for playerStatusView.xaml
    /// </summary>
    [Serializable]
    public partial class PlayerStatusView : UserControl
    {
        GameModel gm;
        MainWindow mw;

        public PlayerStatusView(GameModel gm, MainWindow mw)
        {
            this.gm = gm;
            this.mw = mw;
            InitializeComponent();
            updateView();
        }
        public void updateView()
        {
            playerNameLabel.Content = gm.player.getName();
            cashAmountLabel.Content = gm.player.getCurrentCash().ToString();

            double betAmount = 0;
            foreach (Hand hand in gm.player.getHands())
            {
                betAmount += hand.getBet();
            }
            currentBetAmountLabel.Content = betAmount.ToString();
        }
    }
}
