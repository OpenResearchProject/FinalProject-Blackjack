using System;
using System.Collections.Generic;

namespace Blackjack
{
    [Serializable]
    public class Deck
	{
        public const int NumSuits = 4;
        public const int CardsPerSuit = 13;
        private Dictionary<Suit, List<PlayingCard>> cardDeck;
        private Random randomCardSelector = new Random();

        private DiscardPile discard;

		public Deck()
		{
            //initialize discard
            discard = new DiscardPile();

            this.cardDeck = new Dictionary<Suit, List<PlayingCard>>(NumSuits);

            for (Suit suit = Suit.Clubs; suit <= Suit.Spades; suit++)
            {
                List<PlayingCard> cardsInSuit = new List<PlayingCard>(CardsPerSuit);
                for (Value value = Value.Two; value <= Value.Ace; value++)
                {
                    cardsInSuit.Add(new PlayingCard(suit, value));
                }
                this.cardDeck.Add(suit, cardsInSuit);
            }
		}

        public void shuffleDeck()
        {
            List<PlayingCard> clubs;
            List<PlayingCard> spades;
            List<PlayingCard> hearts;
            List<PlayingCard> diamonds;

            cardDeck.TryGetValue(Suit.Clubs, out clubs);
            cardDeck.TryGetValue(Suit.Diamonds, out diamonds);
            cardDeck.TryGetValue(Suit.Hearts, out hearts);
            cardDeck.TryGetValue(Suit.Spades, out spades);

            foreach (PlayingCard card in discard.getCards())
            {
                if (card.CardSuit == Suit.Clubs)
                {
                    clubs.Add(card);
                }
                if (card.CardSuit == Suit.Diamonds)
                {
                    diamonds.Add(card);
                }
                if (card.CardSuit == Suit.Hearts)
                {
                    hearts.Add(card);
                }
                if (card.CardSuit == Suit.Spades)
                {
                    spades.Add(card);
                }
            }

            discard.clearDiscard();
        }

        public PlayingCard DealCardFromDeck()
        {
            Suit suit = (Suit)randomCardSelector.Next(NumSuits);

            while (this.IsSuitEmpty(suit))
            {
                suit = (Suit)randomCardSelector.Next(NumSuits);
            }

            Value value = (Value)randomCardSelector.Next(CardsPerSuit);
            while (this.IsCardAlreadyDealt(suit, value))
            {
                value = (Value)randomCardSelector.Next(CardsPerSuit);
            }

            List<PlayingCard> cardsInSuit = this.cardDeck[suit];
            PlayingCard card = cardsInSuit.Find(c => c.CardValue == value);
            cardsInSuit.Remove(card);

            shuffleCheck();

            return card;
        }

        private bool IsSuitEmpty(Suit suit)
        {
            bool result = true;
            for (Value value = Value.Two; value <= Value.Ace; value++)
            {
                if (!IsCardAlreadyDealt(suit, value))
                {
                    result = false;
                    break;
                }
            }

            return result;

        }

        private bool IsCardAlreadyDealt(Suit suit, Value value)
        {
            List<PlayingCard> cardsInSuit = this.cardDeck[suit];
            return (!cardsInSuit.Exists(c => c.CardSuit == suit && c.CardValue == value));
        }

        public void addToDiscard(List<PlayingCard> cards)
        {
            discard.addToDiscard(cards);
        }

        public void shuffleCheck()
        {
            if (IsSuitEmpty(Suit.Clubs) && IsSuitEmpty(Suit.Diamonds) && IsSuitEmpty(Suit.Hearts) && IsSuitEmpty(Suit.Spades))
            {
                shuffleDeck();
            }
        }

        public void updateRandomizer(Random rando)
        {
            randomCardSelector = rando;
        }
    }
}